<!-- writeme -->
Drutopia Microblog
==================

Drutopia Microblog helps you publish short posts on your site and syndicate variants of your content to social media.  Does not require Drutopia but integrates well with it.

 * https://www.drupal.org/project/drutopia_microblog
 * Issues: https://www.drupal.org/project/issues/drutopia_microblog
 * Source code: https://git.drupalcode.org/project/drutopia_microblog
 * Keywords: microblog, posts, content, campaign, grassroots, social media, indieweb, mastodon, drutopia
 * Package name: drupal/drutopia_microblog


### Requirements

 * drupal/indieweb ^1.19
 * drupal/textarea_widget_for_text ^1.2


### License

GPL-2.0+

<!-- endwriteme -->
